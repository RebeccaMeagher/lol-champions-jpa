package com.training.lolchampion.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class LolChampion {
    @Id
    @GeneratedValue
    @Column(name="id")
    private Long id;
    @Column(name="name")
    private String name;
    @Column(name="role")
    private String role;
    @Column(name="difficulty")
    private String difficulty;

    @Override
    public String toString(){
        return "LolChampion{" +
                "id=" +id +
                ", name='" + name + '\''+
                ", difficulty='" + difficulty + '\''+
                '}';
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name
    }

    public void setName(String name){
        this.name=name;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role){
        this.role =role;
    }

    public String getDifficulty() {
        return difficulty;
    }

    public void setDifficulty(String difficulty){
        this.difficulty =difficulty;
    }
}