package com.example.lolchampion;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LolchampionApplication {

    public static void main(String[] args) {
        SpringApplication.run(LolchampionApplication.class, args);
    }

}
